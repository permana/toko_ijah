package model

type SkuReportFormat struct {
	Date          string     `json:"date"`
	TotalSku      int        `json:"totalSku"`
	TotalQuantity int64      `json:"totalQuantity"`
	Total         string     `json:"total"`
	Data          SkuReports `json:"data"`
}

type SkuReport struct {
	Sku      string `json:"sku"`
	Name     string `json:"name"`
	Quantity int64  `json:"quantity"`
	Price    string `json:"price"`
	Total    string `json:"total"`
}

type SkuReports []SkuReport

type OrderReportFormat struct {
	Date          string       `json:"date"`
	DatePeriode   string       `json:"datePeriode"`
	TotalOmzet    string       `json:"totalOmzet"`
	TotalRevenue  string       `json:"totalRevenue"`
	TotalOrder    int          `json:"totalOrder"`
	TotalQuantity int64        `json:"totalQuantity"`
	Data          OrderReports `json:"data"`
}

type OrderReport struct {
	ID           string `json:"id"`
	Date         string `json:"date"`
	Sku          string `json:"sku"`
	Name         string `json:"name"`
	Quantity     int64  `json:"quantity"`
	SellingPrice string `json:"sellingPrice"`
	Total        string `json:"total"`
	Price        string `json:"price"`
	Revenue      string `json:"revenue"`
}

type OrderReports []OrderReport
