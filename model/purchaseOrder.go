package model

import (
	"fmt"
	"time"

	"toko_ijah/db"
)

type PurchaseOrder struct {
	Id                   int64                 `json:"Id"`
	Date                 string                `json:"date"`
	ReceiptNumber        string                `json:"receiptNumber"`
	PurchaseOrderDetails []PurchaseOrderDetail `json:"purchaseOrderDetails"`
	CreatedAt            string                `json:"createdAt"`
}
type PurchaseOrderDetail struct {
	Id                              int64                            `json:"Id"`
	PurchaseOrderId                 int64                            `json:"purchaseOrderId"`
	SkuNumber                       string                           `json:"skuNumber"`
	SkuName                         string                           `json:"skuName"`
	TotalOrder                      int64                            `json:"totalOrder"`
	TotalReceived                   int64                            `json:"totalReceived"`
	Price                           int64                            `json:"price"`
	PurchaseOrderDetailTransactions []PurchaseOrderDetailTransaction `json:"purchaseOrderDetailTransactions"`
}

type PurchaseOrderDetailTransaction struct {
	Id                    int64  `json:"Id"`
	PurchaseOrderDetailId int64  `json:"purchaseOrderDetailId"`
	Date                  string `json:"date"`
	QuantityReceived      int64  `json:"quantityReceived"`
}

type POTransform struct {
	Waktu          string `json:"waktu"`
	Sku            string `json:"sku"`
	NamaBarang     string `json:"namaBarang"`
	JumlahPesanan  int64  `json:"jumlahPesanan"`
	JumlahDiterima int64  `json:"jumlahDiterima"`
	HargaBeli      int64  `json:"hargaBeli"`
	Total          int64  `json:"total"`
	Kwitansi       string `json:"kwitansi"`
	Catatan        string `json:"catatan"`
}

type PurchaseOrderList []PurchaseOrder

type POFEFormat []POTransform

func (pos PurchaseOrderList) Transform() POFEFormat {
	var Result POFEFormat
	for _, po := range pos {
		for _, poDetail := range po.PurchaseOrderDetails {
			poData := &POTransform{}
			poData.Waktu = po.Date
			poData.Sku = poDetail.SkuNumber
			poData.NamaBarang = poDetail.SkuName
			poData.JumlahPesanan = poDetail.TotalOrder
			poData.JumlahDiterima = poDetail.TotalReceived
			poData.HargaBeli = poDetail.Price
			poData.Total = poDetail.TotalOrder * poDetail.Price
			poData.Kwitansi = po.ReceiptNumber
			poData.Catatan = poNotesCreator(poDetail.TotalOrder, poDetail.PurchaseOrderDetailTransactions)
			Result = append(Result, *poData)
		}
	}
	return Result
}

func (po *PurchaseOrder) Save() error {
	database := db.GetDB()
	defer database.Close()
	fmt.Println("Start save")
	query := `INSERT INTO purchaseOrders (date, receiptNumber, createdAt) VALUES (?,?,?)`
	statement, err := database.Prepare(query)
	if err != nil {
		fmt.Println("error when prepare")
		panic(err)
	}
	res, err := statement.Exec(po.Date, po.ReceiptNumber, po.CreatedAt)
	if err != nil {
		fmt.Println("error when execute")
		panic(err)
	}
	poId, err := res.LastInsertId()
	err = savePoDetails(poId, po.PurchaseOrderDetails)
	return err
}

func (podt *PurchaseOrderDetailTransaction) Save() error {
	database := db.GetDB()
	defer database.Close()
	fmt.Println("Start save")
	query := `INSERT INTO purchaseOrderDetailTransactions (purchaseOrderDetailId, date, quantityReceived) VALUES (?,?,?)`
	statement, err := database.Prepare(query)
	if err != nil {
		panic(err)
	}
	_, err = statement.Exec(podt.PurchaseOrderDetailId, podt.Date, podt.QuantityReceived)
	if err != nil {
		panic(err)
	}
	updatePoDetails(podt.PurchaseOrderDetailId, podt.QuantityReceived)
	return err
}

// Public Function
func ListPO() PurchaseOrderList {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from purchaseOrders`)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var po_list PurchaseOrderList
	for res.Next() {
		po := &PurchaseOrder{}
		err := res.Scan(&po.Id, &po.Date, &po.ReceiptNumber, &po.CreatedAt)
		fmt.Println(po)
		po.PurchaseOrderDetails = getPODetails(po.Id)
		po_list = append(po_list, *po)
		if err != nil {
			panic(err)
		}
	}
	return po_list
}

func GetPODetailsBySku(skuNumber string) []PurchaseOrderDetail {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from purchaseOrderDetails where skuNumber = "%s"`, skuNumber)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var pod_list []PurchaseOrderDetail
	for res.Next() {
		pod := &PurchaseOrderDetail{}
		err := res.Scan(&pod.Id, &pod.PurchaseOrderId, &pod.SkuNumber, &pod.SkuName, &pod.TotalOrder, &pod.TotalReceived, &pod.Price)
		pod.PurchaseOrderDetailTransactions = getPODetailTransactions(pod.Id)
		pod_list = append(pod_list, *pod)
		if err != nil {
			panic(err)
		}
	}
	return pod_list
}

func NewPO(Date string, ReceiptNumber string, PurchaseOrderDetails []PurchaseOrderDetail) *PurchaseOrder {
	return &PurchaseOrder{
		Date:                 Date,
		ReceiptNumber:        ReceiptNumber,
		PurchaseOrderDetails: PurchaseOrderDetails,
		CreatedAt:            time.Now().Format("2006-01-02 15:04:05"),
	}
}

func NewPODetailTransaction(PurchaseOrderDetailId int64, Date string, QuantityReceived int64) *PurchaseOrderDetailTransaction {
	return &PurchaseOrderDetailTransaction{
		PurchaseOrderDetailId: PurchaseOrderDetailId,
		Date:             Date,
		QuantityReceived: QuantityReceived,
	}
}

// private function
func getPO(id int64) PurchaseOrder {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from purchaseOrders where id = %d`, id)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var po PurchaseOrder
	for res.Next() {
		p := &PurchaseOrder{}
		err := res.Scan(&p.Id, &p.Date, &p.ReceiptNumber)
		po = *p
		if err != nil {
			panic(err)
		}
	}
	return po
}

func getPODetails(poId int64) []PurchaseOrderDetail {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from purchaseOrderDetails where purchaseOrderId = %d`, poId)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var pod_list []PurchaseOrderDetail
	for res.Next() {
		pod := &PurchaseOrderDetail{}
		err := res.Scan(&pod.Id, &pod.PurchaseOrderId, &pod.SkuNumber, &pod.SkuName, &pod.TotalOrder, &pod.TotalReceived, &pod.Price)
		pod.PurchaseOrderDetailTransactions = getPODetailTransactions(pod.Id)
		pod_list = append(pod_list, *pod)
		if err != nil {
			panic(err)
		}
	}
	return pod_list
}

func getPODetailTransactions(podId int64) []PurchaseOrderDetailTransaction {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from purchaseOrderDetailTransactions where purchaseOrderDetailId = %d`, podId)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var podt_list []PurchaseOrderDetailTransaction
	for res.Next() {
		podt := &PurchaseOrderDetailTransaction{}
		err := res.Scan(&podt.Id, &podt.PurchaseOrderDetailId, &podt.Date, &podt.QuantityReceived)
		podt_list = append(podt_list, *podt)
		if err != nil {
			panic(err)
		}

	}
	return podt_list
}

func savePoDetails(poId int64, poDetails []PurchaseOrderDetail) error {
	database := db.GetDB()
	defer database.Close()
	var err error
	for _, poDetail := range poDetails {
		query := `INSERT INTO purchaseOrderDetails (purchaseOrderId,skuNumber,skuName,totalOrder,totalReceived,price) VALUES (?,?,?,?,?,?)`
		statement, err := database.Prepare(query)
		if err != nil {
			fmt.Println("error when prepare")
			panic(err)
		}
		_, err = statement.Exec(poId, poDetail.SkuNumber, poDetail.SkuName, poDetail.TotalOrder, 0, poDetail.Price)
		if err != nil {
			fmt.Println("error when execute")
			panic(err)
		} else {
			// check and create if it's new sku
			createSku(poDetail.SkuNumber, poDetail.SkuName)
		}
	}
	return err
}

func updatePoDetails(podtId int64, QuantityReceived int64) error {
	database := db.GetDB()
	defer database.Close()
	stmt, err := database.Prepare("update purchaseOrderDetails set totalReceived=CAST(totalReceived as INT)+? where id=?")
	if err != nil {
		panic(err)
	}
	res, err := stmt.Exec(QuantityReceived, podtId)
	fmt.Println(res)
	if err != nil {
		panic(err)
	}
	query := fmt.Sprintf(`select skuNumber from purchaseOrderDetails where id = %d`, podtId)
	var skuNumber string
	fmt.Printf(query)
	err = database.QueryRow(query).Scan(&skuNumber)
	fmt.Println(skuNumber)
	AddSkuQty(skuNumber, QuantityReceived)
	return err
}

func poNotesCreator(target int64, podt []PurchaseOrderDetailTransaction) string {
	var result string
	totalReceived := int64(0)
	for _, tx := range podt {
		result += fmt.Sprintf("%s diterima %d; ", tx.Date, tx.QuantityReceived)
		totalReceived += tx.QuantityReceived
	}
	if totalReceived < target {
		result += "Masih Menunggu"
	}
	return result
}
