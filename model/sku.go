package model

import (
	"fmt"
	"toko_ijah/db"

	_ "github.com/mattn/go-sqlite3"
)

type Sku struct {
	SkuNumber string `json:"skuNumber"`
	SkuName   string `json:"skuName"`
	Quantity  int64  `json:"quantity"`
}

type Skus []Sku

func NewSku(SkuNumber string, SkuName string, Quantity int64) *Sku {
	return &Sku{
		SkuNumber: SkuNumber,
		SkuName:   SkuName,
		Quantity:  Quantity,
	}
}

func GetInventories() Skus {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select skuNumber, skuName, quantity from sku`)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var sku_list Skus
	for res.Next() {
		s := &Sku{}
		err := res.Scan(&s.SkuNumber, &s.SkuName, &s.Quantity)
		sku_list = append(sku_list, *s)
		if err != nil {
			panic(err)
		}

	}
	return sku_list
}

func AddSkuQty(skuNumber string, qty int64) {
	database := db.GetDB()
	defer database.Close()
	stmt, err := database.Prepare("update sku set quantity=CAST(quantity as INT)+? where skuNumber=?")
	if err != nil {
		panic(err)
	}
	res, err := stmt.Exec(qty, skuNumber)
	fmt.Println(res)
	if err != nil {
		panic(err)
	}
}

func ReduceSkuQty(skuNumber string, qty int64) {
	database := db.GetDB()
	defer database.Close()
	stmt, err := database.Prepare("update sku set quantity=CAST(quantity as INT)-? where skuNumber=?")
	if err != nil {
		panic(err)
	}
	res, err := stmt.Exec(qty, skuNumber)
	fmt.Println(res)
	if err != nil {
		panic(err)
	}
}

// func GetSkuBySkuNumber(skuNumber string) (*Sku, error) {
// 	database := db.GetDB()
// 	defer database.Close()
// 	query := fmt.Sprintf(`select skuNumber,skuName,quantity from sku where skuNumber = "%s" limit 1`, skuNumber)
// 	fmt.Println(query)
// 	var s Sku
// 	err := database.QueryRow(query).Scan(&s.SkuNumber, s.SkuName, s.Quantity)
// 	if err != nil {
// 		return &Sku{}, err
// 	} else {
// 		return &s, nil
// 	}
// }

func skuExist(skuNumber string) bool {
	// check if sku exist based in their sku number
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select count(skuNumber) as count from sku where skuNumber = "%s"`, skuNumber)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var count int
	for res.Next() {
		err := res.Scan(&count)
		if err != nil {
			panic(err)
		}
	}
	if count > 0 {
		return true
	} else {
		return false
	}
}

func createSku(skuNumber string, skuName string) {
	database := db.GetDB()
	defer database.Close()
	isSkuExist := skuExist(skuNumber)
	if isSkuExist == false {
		query := `INSERT INTO sku (skuNumber,skuName,quantity) VALUES (?,?,?)`
		statement, err := database.Prepare(query)
		if err != nil {
			panic(err)
		}
		_, err = statement.Exec(skuNumber, skuName, 0)
		if err != nil {
			panic(err)
		}
	}
}
