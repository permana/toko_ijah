package model

import (
	"fmt"
	"toko_ijah/db"
)

type Order struct {
	Id           int64         `json:"id"`
	Date         string        `json:"date"`
	OrderNumber  string        `json:"orderNumber"`
	Notes        string        `json:"notes"`
	OrderType    string        `json:"orderType"`
	OrderDetails []OrderDetail `json:"orderDetails"`
}

type OrderList []Order

type OrderDetail struct {
	Id        int64  `json:"id"`
	OrderId   int64  `json:"orderId"`
	SkuNumber string `json:"skuNumber"`
	SkuName   string `json:"skuName"`
	Quantity  int64  `json:"quantity"`
	Price     int64  `json:"price"`
}

func ListOrder() OrderList {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from orders where config = "order"`)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var order_list OrderList
	for res.Next() {
		o := &Order{}
		err := res.Scan(&o.Id, &o.OrderNumber, &o.Date, &o.Notes, &o.OrderType)
		o.OrderDetails = getOrderDetails(o.Id)
		order_list = append(order_list, *o)
		if err != nil {
			panic(err)
		}
	}
	return order_list
}
func ListOrderAndOther() OrderList {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from orders`)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var order_list OrderList
	for res.Next() {
		o := &Order{}
		err := res.Scan(&o.Id, &o.OrderNumber, &o.Date, &o.Notes, &o.OrderType)
		o.OrderDetails = getOrderDetails(o.Id)
		order_list = append(order_list, *o)
		if err != nil {
			panic(err)
		}
	}
	return order_list
}

func getOrderDetails(oId int64) []OrderDetail {
	database := db.GetDB()
	defer database.Close()
	query := fmt.Sprintf(`select * from orderDetails where orderId = %d`, oId)
	fmt.Println(query)
	res, err := database.Query(query)
	if err != nil {
		panic(err)
	}
	var od_list []OrderDetail
	for res.Next() {
		od := &OrderDetail{}
		err := res.Scan(&od.Id, &od.OrderId, &od.SkuNumber, &od.SkuName, &od.Quantity, &od.Price)
		od_list = append(od_list, *od)
		if err != nil {
			panic(err)
		}
	}
	return od_list
}

func NewOrder(Date string, OrderNumber string, Notes string, OrderType string, OrderDetails []OrderDetail) *Order {
	return &Order{
		Date:         Date,
		OrderNumber:  OrderNumber,
		Notes:        Notes,
		OrderType:    OrderType,
		OrderDetails: OrderDetails,
	}
}

func (o *Order) Save() error {
	database := db.GetDB()
	defer database.Close()
	query := `INSERT INTO orders (date, orderNumber, notes, orderType) VALUES (?,?,?,?)`
	statement, err := database.Prepare(query)
	if err != nil {
		panic(err)
	}
	res, err := statement.Exec(o.Date, o.OrderNumber, o.Notes, o.OrderType)
	if err != nil {
		panic(err)
	}
	oId, err := res.LastInsertId()
	err = saveOrderDetails(oId, o.OrderDetails)
	return err
}

func saveOrderDetails(oId int64, orderDetails []OrderDetail) error {
	database := db.GetDB()
	defer database.Close()
	var err error
	for _, orderDetail := range orderDetails {
		query := `INSERT INTO orderDetails (orderId,skuNumber,skuName,quantity,price) VALUES (?,?,?,?,?)`
		statement, err := database.Prepare(query)
		if err != nil {
			panic(err)
		}
		_, err = statement.Exec(oId, orderDetail.SkuNumber, orderDetail.SkuName, orderDetail.Quantity, orderDetail.Price)
		if err != nil {
			panic(err)
		} else {
			ReduceSkuQty(orderDetail.SkuNumber, orderDetail.Quantity)
		}
	}
	return err
}

type OrderFEFormat []OrderTransform
type OrderTransform struct {
	Waktu        string `json:"waktu"`
	Sku          string `json:"sku"`
	NamaBarang   string `json:"namaBarang"`
	JumlahKeluar int64  `json:"jumlahKeluar"`
	HargaJual    int64  `json:"hargaJual"`
	Total        int64  `json:"total"`
	Catatan      string `json:"catatan"`
}

func (orders OrderList) Transform() OrderFEFormat {
	var Result OrderFEFormat
	for _, o := range orders {
		fmt.Printf("%v", o)
		for _, od := range o.OrderDetails {
			ot := &OrderTransform{}
			ot.Waktu = o.Date
			ot.Sku = od.SkuNumber
			ot.NamaBarang = od.SkuName
			ot.JumlahKeluar = od.Quantity
			ot.HargaJual = od.Price
			ot.Total = od.Quantity * od.Price
			ot.Catatan = o.Notes
			Result = append(Result, *ot)
		}
	}
	return Result
}
