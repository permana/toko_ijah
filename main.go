package main

import (
	"toko_ijah/route"
)

func main() {

	router := route.Init()
	router.Logger.Fatal(router.Start(":1323"))
}
