package db

func initialTable() []string {
	queries := []string{
		purchaseOrderTable,
		purchaseOrderDetailTable,
		purchaseOrderDetailTransactionTable,
		orderTable,
		orderDetailTable,
		skuTable,
	}
	return queries
}

const (
	purchaseOrderTable string = `CREATE TABLE IF NOT EXISTS purchaseOrders 
                        (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                        date TEXT, 
                        receiptNumber TEXT, 
                        createdAt TEXT)`
	purchaseOrderDetailTable string = `CREATE TABLE IF NOT EXISTS purchaseOrderDetails 
                        (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                        purchaseOrderId INTEGER, 
                        skuNumber TEXT, 
                        skuName TEXT, 
                        totalOrder TEXT,
                        totalReceived INTEGER,
                        price INTEGER)`
	purchaseOrderDetailTransactionTable string = `CREATE TABLE IF NOT EXISTS purchaseOrderDetailTransactions 
                        (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                        purchaseOrderDetailId INTEGER, 
                        date TEXT, 
                        quantityReceived INTEGER)`
	orderTable string = `CREATE TABLE IF NOT EXISTS orders 
                        (id INTEGER PRIMARY KEY AUTOINCREMENT,
                        orderNumber TEXT,
                        date TEXT, 
                        notes TEXT,
                        orderType TEXT)`
	orderDetailTable string = `CREATE TABLE IF NOT EXISTS orderDetails
                        (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                        orderId INTEGER, 
                        skuNumber TEXT,
                        skuName TEXT,
                        quantity INTEGER,
                        price INTGEER)`
	skuTable string = `CREATE TABLE IF NOT EXISTS sku
                        (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                        skuNumber TEXT, 
                        skuName TEXT,
                        quantity INTEGER)`
)
