package db

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func Init() {
	database, _ := sql.Open("sqlite3", "./toko_ijah.db")
	tables := initialTable()
	for _, table := range tables {
		statement, err := database.Prepare(table)
		if err != nil {
			fmt.Println("error when prepare create table")
			fmt.Println(table)
			fmt.Println(err)
		}
		res, err := statement.Exec()
		fmt.Println(res)
		if err != nil {
			fmt.Println("error when create table")
			fmt.Println(err)
		}
		fmt.Println(table)
	}
}

func GetDB() *sql.DB {
	database, _ := sql.Open("sqlite3", "./toko_ijah.db")
	return database
}
