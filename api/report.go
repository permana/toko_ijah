package api

import (
	"toko_ijah/repo"

	"github.com/labstack/echo"
)

func InventoryReport() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		report := repo.GetReportSku()
		return c.JSON(200, report)
	}
}

func InventoryReportExport() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		repo.ExportReportSku()
		return c.File("./reports/sku.csv")
	}
}

func SellingReport() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		report := repo.GetReportOrder()
		return c.JSON(200, report)
	}
}

func SellingReportExport() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		repo.ExportReportOrder()
		return c.File("./reports/order.csv")
	}
}
