package api

import (
	"toko_ijah/model"

	"github.com/labstack/echo"
)

func GetInventoryList() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		Skus := model.GetInventories()
		return c.JSON(200, Skus)
	}
}

// Barang Masuk
func GetPoList() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		ListPO := model.ListPO()
		return c.JSON(200, ListPO.Transform())
	}
}

func SavePO() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		m := new(model.PurchaseOrder)
		c.Bind(&m)
		PO := model.NewPO(m.Date, m.ReceiptNumber, m.PurchaseOrderDetails)
		PO.Save()
		return c.JSON(200, PO)
	}
}

func SaveFullfilment() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		m := new(model.PurchaseOrderDetailTransaction)
		c.Bind(&m)
		PODetailTransaction := model.NewPODetailTransaction(m.PurchaseOrderDetailId, m.Date, m.QuantityReceived)
		PODetailTransaction.Save()
		return c.JSON(200, PODetailTransaction)
	}
}

// Barang Keluar
func GetOrderList() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		orders := model.ListOrderAndOther()
		return c.JSON(200, orders.Transform())
	}
}

func SaveOrder() echo.HandlerFunc {
	return func(c echo.Context) (err error) {
		m := new(model.Order)
		c.Bind(&m)
		PO := model.NewOrder(m.Date, m.OrderNumber, m.Notes, m.OrderType, m.OrderDetails)
		PO.Save()
		return c.JSON(200, PO)
	}
}
