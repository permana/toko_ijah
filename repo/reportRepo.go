package repo

import (
	"encoding/csv"
	"os"
	"strconv"
	"time"
	"toko_ijah/helper"
	"toko_ijah/model"
)

func ExportReportSku() bool {
	file, err := os.OpenFile("./reports/sku.csv", os.O_CREATE|os.O_WRONLY, 0777)
	defer file.Close()
	if err != nil {
		return false
	}
	report := GetReportSku()
	writer := csv.NewWriter(file)
	title := []string{"LAPORAN NILAI BARANG"}
	h1 := []string{"Tanggal Cetak", report.Date}
	h2 := []string{"Jumlah Sku", strconv.Itoa(report.TotalSku)}
	h3 := []string{"Jumlah Total Barang", strconv.Itoa(int(report.TotalQuantity))}
	h4 := []string{"Jumlah Nilai", report.Total}

	writer.Write(title)
	writer.Write([]string{})
	writer.Write(h1)
	writer.Write(h2)
	writer.Write(h3)
	writer.Write(h4)
	writer.Write([]string{})
	writer.Write([]string{"SKU", "Nama Item", "Jumlah", "Rata-Rata Harga Beli", "Total"})
	for _, data := range report.Data {
		s := []string{data.Sku, data.Name, strconv.Itoa(int(data.Quantity)), data.Price, data.Total}
		writer.Write(s)
	}
	writer.Flush()
	return true
}

func ExportReportOrder() bool {
	file, err := os.OpenFile("./reports/order.csv", os.O_CREATE|os.O_WRONLY, 0777)
	defer file.Close()
	if err != nil {
		return false
	}
	report := GetReportOrder()
	writer := csv.NewWriter(file)
	title := []string{"LAPORAN PENJUALAN"}
	h1 := []string{"Tanggal Cetak", report.Date}
	h2 := []string{"Tanggal", report.DatePeriode}
	h3 := []string{"Total Omzet", report.TotalOmzet}
	h4 := []string{"Total Laba Kotor", report.TotalRevenue}
	h5 := []string{"Total Penjualan", strconv.Itoa(report.TotalOrder)}
	h6 := []string{"Total Barang", strconv.Itoa(int(report.TotalQuantity))}

	writer.Write(title)
	writer.Write([]string{})
	writer.Write(h1)
	writer.Write(h2)
	writer.Write(h3)
	writer.Write(h4)
	writer.Write(h5)
	writer.Write(h6)
	writer.Write([]string{})
	writer.Write([]string{"ID Pesanan", "Waktu", "SKU", "Nama Barang", "Jumlah", "Harga Jual", "Total", "Harga Beli", "Laba"})
	for _, data := range report.Data {
		s := []string{data.ID, data.Date, data.Sku, data.Name, strconv.Itoa(int(data.Quantity)), data.SellingPrice, data.Total, data.Price, data.Revenue}
		writer.Write(s)
	}
	writer.Flush()
	return true
}

func GetReportSku() model.SkuReportFormat {
	var result model.SkuReportFormat
	Skus := model.GetInventories()
	var skuReports model.SkuReports
	totalQuantity := int64(0)
	totalValue := int64(0)
	for _, s := range Skus {
		avgPrice := getAvgPrice(s.SkuNumber)
		sku := &model.SkuReport{}
		sku.Sku = s.SkuNumber
		sku.Name = s.SkuName
		sku.Price = "Rp. " + helper.MoneyFormat(avgPrice)
		sku.Quantity = s.Quantity
		sku.Total = "Rp. " + helper.MoneyFormat(avgPrice*s.Quantity)
		skuReports = append(skuReports, *sku)
		totalQuantity += s.Quantity
		totalValue += avgPrice * s.Quantity
	}
	result.Date = time.Now().Format("2006-01-02 15:04:05")
	result.TotalSku = len(Skus)
	result.TotalQuantity = totalQuantity
	result.Total = "Rp. " + helper.MoneyFormat(totalValue)
	result.Data = skuReports
	return result
}

func GetReportOrder() model.OrderReportFormat {
	var result model.OrderReportFormat
	orders := model.ListOrder()
	var orderReports model.OrderReports
	totalOmzet := int64(0)
	totalRevenue := int64(0)
	totalQuantity := int64(0)
	for _, o := range orders {
		for _, od := range o.OrderDetails {
			avgPrice := getAvgPrice(od.SkuNumber)
			or := &model.OrderReport{}
			or.ID = o.OrderNumber
			or.Date = o.Date
			or.Sku = od.SkuNumber
			or.Name = od.SkuName
			or.Quantity = od.Quantity
			or.SellingPrice = "Rp. " + helper.MoneyFormat(od.Price)
			or.Total = "Rp. " + helper.MoneyFormat(od.Quantity*od.Price)
			or.Price = "Rp. " + helper.MoneyFormat(avgPrice)
			or.Revenue = "Rp. " + helper.MoneyFormat((od.Price*od.Quantity)-(od.Quantity*avgPrice))
			orderReports = append(orderReports, *or)
			totalOmzet += od.Quantity * od.Price
			totalQuantity += od.Quantity
			totalRevenue += (od.Price * od.Quantity) - (od.Quantity * avgPrice)
		}
	}
	result.Date = time.Now().Format("2006-01-02 15:04:05")
	// result.DatePeriode = berdasarkan tanggal awal dan akhir dari parameter bulan
	result.TotalOmzet = "Rp. " + helper.MoneyFormat(totalOmzet)
	result.TotalRevenue = "Rp. " + helper.MoneyFormat(totalRevenue)
	result.TotalOrder = len(orders)
	result.TotalQuantity = totalQuantity
	result.Data = orderReports
	return result
}

func getAvgPrice(skuNumber string) int64 {
	var result int64
	poDetails := model.GetPODetailsBySku(skuNumber)
	var totalPrice int64
	for _, pod := range poDetails {
		totalPrice += pod.Price
	}
	result = int64(totalPrice / int64(len(poDetails)))
	return result
}
