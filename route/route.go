package route

import (
	"toko_ijah/api"
	"toko_ijah/db"

	"github.com/labstack/echo"
	echoMw "github.com/labstack/echo/middleware"
)

func Init() *echo.Echo {

	e := echo.New()

	// Set Bundle MiddleWare
	e.Use(echoMw.Logger())
	e.Use(echoMw.Gzip())
	e.Use(echoMw.CORSWithConfig(echoMw.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAcceptEncoding},
	}))
	db.Init()
	// Routes
	v1 := e.Group("/api/v1")
	{
		// Inventory
		v1.GET("/list_sku", api.GetInventoryList())
		// Barang masuk
		v1.POST("/create_po", api.SavePO())
		v1.POST("/fullfilment", api.SaveFullfilment())
		v1.GET("/list_po", api.GetPoList())
		// Barang Keluar
		v1.POST("/order", api.SaveOrder())
		v1.GET("/list_order", api.GetOrderList())
		// Laporan
		report := v1.Group("/report")
		{
			// Need export function
			report.GET("/sku", api.InventoryReport())
			report.GET("/sku/export", api.InventoryReportExport())
			report.GET("/order", api.SellingReport())
			report.GET("/order/export", api.SellingReportExport())
		}
	}

	return e
}
