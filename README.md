
### Requirement
- Echo Framework (https://echo.labstack.com/)
- Glide (https://github.com/Masterminds/glide)

  

### Assumption :
- Kwitansi tidak mandatory
- Seller harus membuat data pemesanan terlebih dahulu baru setelah itu memasukan data barang masuk sesuai dengan data pemesanan yang sudah dibuat sebelumnya
- semua validasi dilakukan do frontend
- 

### Endpoint list 
```
- [POST] http://localhost:1323/api/v1/create_po
- [POST] http://localhost:1323/api/v1/fullfilment
- [POST] http://localhost:1323/api/v1/order
- [GET] http://localhost:1323/api/v1/list_sku
- [GET] http://localhost:1323/api/v1/list_po
- [GET] http://localhost:1323/api/v1/list_order
- [GET] http://localhost:1323/api/v1/report/sku
- [GET] http://localhost:1323/api/v1/report/order
- [GET] http://localhost:1323/api/v1/report/sku/export
- [GET] http://localhost:1323/api/v1/report/order/export

```
### Sample Request Create Purchase Order
```
{
	"date":"2017/12/15 1:48",
	"receiptNumber":"20171215-52086",
	"purchaseOrderDetails":[
		{
		"skuNumber":"SSI-D00864652-SS-NAV",
		"skuName":"Deklia Plain Casual Blouse (S,Navy)",
		"totalOrder":28,
		"totalReceived":0,
		"price":63000
		}
	]
}
```

### Sample Request fullfilment
```
{
	"purchaseOrderDetailId":1,
	"date":"2018/01/02 11:20",
	"quantityReceived":15
}
```

### Sample Request order
```
{
	"orderNumber":"ID-20171130-531154",
	"date": "2018/01/02 11:20",
	"orderDetails": [
		{
		"skuNumber": "SSI-D00864652-SS-NAV",
		"skuName": "Deklia Plain Casual Blouse (S,Navy)",
		"quantity": 3,
		"price": 80000
		}
	]
}
```

### How To Run
- Clone this repo
- Go to the main folder
- Run this command on terminal
``
./main
``